# My profile Scripts.
This repo is just a collection of my automation scripts that I seem to rarely maintain.

# Installation
To install this profile to your linux system you just clone this repo and run the profile.sh and follow the instructions.

```sh
git clone https://gitlab.com/Barr3tt/profile && cd profile
chmod +x ./profile.sh && ./profile.sh
```
Once the script installs ohmyzsh and opens a zsh shell. Just exit the shell with `exit`.
