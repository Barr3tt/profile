#!/bin/bash
#Find out what Distro
. /etc/os-release
echo $ID_LIKE
if [[ ! -d ~/.git ]]
then
	mkdir ~/.git
fi
#Install Software
function software {
	case $ID_LIKE in
		rhel*)
			sudo dnf update -y
			flatpak update -y
			sudo dnf install -y git curl python3 neofetch fortune-mod zsh
			#Non distro specific functions
			#dotfiles
			#shelltools
			#gittools
		;;	
		debian*)
			sudo apt update && sudo apt upgrade -y
			sudo apt install git curl python3 neofetch fortune-mod zsh
			#Non distro specific functions
			#dotfiles
			#shelltools
			#gittools
		;;
		ubuntu*)
			sudo apt update && sudo apt upgrade -y
			sudo apt install git curl python3 neofetch fortune-mod zsh
			#Non distro specific functions
			#dotfiles
			#shelltools
			#gittools
		;;
		*)
			echo "Your distribution is not supported..."
		;;
	esac
}
function virtualization {
	case $ID in
		fedora*)
			sudo dnf update -y
			sudo dnf install -y qemu virt-manager virt-viewer dnsmasq bridge-utils netcat swtpm edk2-ovmf
		;;	
		debian*)
			echo "Untested!!!"
			sudo apt update && sudo apt upgrade -y
			sudo apt install -y qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils netcat swtpm ovmf
		;;
		ubuntu*)
			sudo apt update && sudo apt upgrade -y
			sudo apt install -y qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils netcat swtpm ovmf
		;;
		arch*)
			sudo pacman -Syyuu
			sudo pacman -Sy qemu virt-manager virt-viewer dnsmasq vde2 bridge-utils openbsd-netcat swtpm ovmf
		;;
		*)
			echo "Your distribution is not supported..."
		;;
	esac
	sudo usermod -aG libvirt $(whoami)
	sudo systemctl enable libvirtd
	sudo systemctl start libvirt
}
function dotfiles {
  	echo "pokemon-colorscripts -r" >> ~/.zshrc
	echo "fortune -s -e" >> ~/.zshrc
}
function shelltools {
	zsh <(curl -s https://raw.githubusercontent.com/zap-zsh/zap/master/install.zsh) --branch release-v1
}
function gittools {
	cd .git
	#Pokemon Script
	git clone https://gitlab.com/phoneybadger/pokemon-colorscripts.git && cd pokemon-colorscripts && sudo ./install.sh && cd ..
	
	cd
}
software
